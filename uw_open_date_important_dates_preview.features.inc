<?php

/**
 * @file
 * uw_open_date_important_dates_preview.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_open_date_important_dates_preview_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
