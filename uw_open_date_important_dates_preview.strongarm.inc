<?php

/**
 * @file
 * uw_open_date_important_dates_preview.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_open_date_important_dates_preview_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_open_data_important_dates_show_preview';
  $strongarm->value = '1';
  $export['uw_open_data_important_dates_show_preview'] = $strongarm;

  return $export;
}
